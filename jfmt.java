//Author: Rahul Palamuttam, rpalamut@ucsc.edu
// $Id: jfmt.java,v 1.31 2013-10-07 17:01:54-07 - - $
//
//NAME
//   jfmt - simple text formatter

//SYNOPSIS
//   jfmt [-width][filename...]

//DESCRIPTION
//   The format utility reads in text lines from all of its input
//   files and writes its output to stdout. Error messages are 
//   written to stderr. Each file is handled separatley, and within
//   each  file, all consecutive sequences of lines containing 
//   non-whitespace characters are considered as a single paragraph.
//   A paragraph is written out with a maximal sequence of words not
//   to exceed the specified output line width. It is then followed 
//   by one empty line.

//OPTIONS
//   -width
//   The specified width is the maximum number of characters in an
//   output line. If a word that is longer than width is found, it
//   is printed on a line by itself. The default width is 65 
//   characters


//OPERANDS
//   Each operand is a file name, and they are read in sequence. An 
//   option is recognized as such only if it begins with a minus sign
//   and precedes all operands. If any operand is specified as a single
//   minus sign (-), then stdin is read at that point.

//EXIT STATUS
//   0   All files were read successfully, output was successfully
//   generated, and no errors were detected.
//   1   Errors were detected and messages were written to stderr

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static java.lang.System.*;


class jfmt {
   // Static variables keeping the general status of the program.
   public static final String JAR_NAME = get_jarname();
   public static final int EXIT_SUCCESS = 0;
   public static final int EXIT_FAILURE = 1;
   public static int exit_status = EXIT_SUCCESS;
   public static StringBuffer buff = new StringBuffer();
   public static int LineLength = 65;

   /* A basename is the final component of a pathname.
   * If a java program is run from a jar, the classpath is the
   * pathname of the jar.
   */
   static String get_jarname() {
    String jarpath = getProperty ("java.class.path");
    int lastslash = jarpath.lastIndexOf ('/');
    if (lastslash < 0) return jarpath;
    return jarpath.substring (lastslash + 1);
   }

   /* Formats a single file making sure to remove excessive newlines,
   * by combining all occurrences of continuous newlines into one.
   * It also formats the output to fit the specified width
   */
   static void format (Scanner infile) {
    List<String> words = new LinkedList<String>();
    String line = ""; 
    line = jumplines(infile, line);
    // Move through each line of the file
    while (infile.hasNextLine()){
      // Checks for continuous empty lines and prints paragraph      
      if (line.isEmpty() && infile.hasNextLine()) {
       line = jumplines(infile, line);
       print_paragraph(words);
       out.println("");
       continue;
      } 
      // iterates over input, adding words to list,
      wordtolist(line, words);
      //changes the scanner pointer and line variable to nextline
      // also removes any trailing whitespaces (deals with tabs)
      line = infile.nextLine().trim();
    }   
    //processes final line
    help_line (line, words);
    print_paragraph(words);
   }

   /* A helper function for format(). It jumps across continuous
   * occurrences of newlines in a file by moving the
   * scanner.nextLine pointer to a nonempty line. It then returns
   * the nonempty line.
   */
   static String jumplines (Scanner infile, String line){
    while(infile.hasNextLine() && line.isEmpty()){
      line = infile.nextLine();
    }
    return line;
   }

   /* wordtolist takes the words in the String parameter and
   * adds them to the List parameter. It is a helper function
   * used by format.
   */
   static void wordtolist (String line, List<String> words){
    // Splits the line into words and adds it to list
    for (String word: line.split("\\s+")){
      // Skip an empty word if such is found.   
      if (word.length() == 0) continue;
      // Append the word to the end of the linked list.
      words.add (word);
    } 
   }

   /* help_line is a helper function for format.  
   * It processes the last line.
   */
   static void help_line (String line, List<String> words){
    if (line.isEmpty()) {
      print_paragraph(words);
      out.println("");
      return;
    } 
    wordtolist(line, words);
   }

   /* print_paragraph is called whenver the format function
   * sees the end of a paragraph, or end of file. It iterates
   * over each word and prints it with a space between words.
   * Finally it emptys the List that was passed to it
   */
   static void print_paragraph (List<String> words){
      // iterate over list, printing paragraph
      for (String word: words){
      buff.append(word);
      // checks if the buffer is longer than it should be
      if(buff.length() > LineLength){
       buff.delete(buff.lastIndexOf(word), buff.length());
       printbuff();
       buff.append(word);
      }
      if(buff.length() > 0) buff.append(" ");
    }
    printbuff();
    //clears the list
    words.clear();
   }

   /* printbuff is a helper function to print_paragraph. 
   * It deletes the last space in the buffer and prints
   * out the buffer if the buffer is not empty. Finally
   * it clears the buffer.
   */
   static void printbuff(){
    // removes a space character if it exists
    if (buff.length() > 0) buff.deleteCharAt(buff.length() -1);
    // prints the buffer if it isn't empty
    if (buff.length() > 0) out.println(buff.toString());
    //clears the buffer and adds the removed word
    buff.delete(0,buff.length());
   }

   // Main function scans arguments and opens/closes files.
   public static void main (String[] args) {
    int argix = 0;
    if (args.length == 0) {
      // There are no filenames given on the command line.
      format (new Scanner (in));
    }else {
      // checks for width parameter and converts string to int
      if (args[0].startsWith("-") && args[0].length() > 1){
       try{
       LineLength = Integer.parseInt(args[0].substring(1));
       } catch (NumberFormatException error) {
          exit_status = EXIT_FAILURE;
             err.printf("Usage: jfmt [-width] [filename...]%n");
             exit (exit_status);
       }
       argix = 1;
      }
      // Iterate over each filename given on the command line.
      for (; argix < args.length; ++argix) {
       String filename = args[argix];
       if (filename.equals ("-")) {
         // Treat a filename of "-" to mean System.in.
         format (new Scanner (in));
       }else {
         // Prints newline as first line - to mimic pfmt.perl
         // Open the file and read it, or error out.
         try {
           Scanner infile = new Scanner (new File (filename));
           //prints newline if file is succesfully loaded
           out.printf ("%n");
           format (infile);
           infile.close();
         }catch (IOException error) {
           exit_status = EXIT_FAILURE;
           err.printf ("%s: %s%n", JAR_NAME,
                error.getMessage());
         }
       }
      }
    }
    exit (exit_status);
   }

}
