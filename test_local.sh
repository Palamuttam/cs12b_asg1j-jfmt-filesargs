FILE=./large_files/*
FILE1=./files/*
ERROR=./large_files/0ws1110.txt
FILE2=./largefiles/0ws1110.txt
FILE3=./files/pfmt.output2
LARGEFILES=./largefiles/*
echo "This is a script used to test diffs of jfmt and pfmt." > output.diff	
for f in $FILE1 $FILE2 $ERROR $FILE3
do
    echo "jfmt $f:" >> output.diff
    ./pfmt.perl $f > output.perl
    ./jfmt $f > output.java
    diff output.perl output.java >> output.diff
    for i in {30..60}
    do
    	./pfmt.perl -$i $f > output.perl
    	./jfmt -$i $f > output.java
    	dif="$(diff output.perl output.java)"
    	if [ "$dif" != "" ]
    	then 
    	    echo "jfmt -$i $f:" >> output.diff
    	fi
    	diff output.perl output.java >> output.diff
    done
done
